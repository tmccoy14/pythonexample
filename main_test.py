import unittest
from main import greeting


class TestGreeting(unittest.TestCase):
    def test_string(self):
        test_greeting = greeting()
        self.assertIn("HELLO", test_greeting, "Hello not found in greeting string.")


if __name__ == "__main__":
    unittest.main()
