FROM python:3.7.4-slim-buster

# set working directory to app
WORKDIR /app

# copy all project contents to directory app
COPY . /app

# pip install/upgrade pip
RUN pip install --upgrade pip

# pip install packages
RUN pip install -r requirements.txt

# run the python scripts
CMD ["python", "main.py"]