"""Standard library"""
import os

"""Third party modules"""
from colorama import Fore, Back, Style


def greeting():

    greeting_string = (
        Fore.RED
        + "HELLO "
        + Fore.GREEN
        + "FROM "
        + Fore.BLUE
        + os.getcwd()
        + Style.RESET_ALL
    )

    return greeting_string


if __name__ == "__main__":
    """Greet the user from the current directory"""

    print(greeting())
